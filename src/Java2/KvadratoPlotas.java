import java.util.Scanner;

public class KvadratoPlotas {
    public static void main(String[] args) {
        System.out.println("Iveskite kvadrato krastine");
        Scanner skaitytuvas = new Scanner(System.in);
        Double krastine = skaitytuvas.nextDouble();
        Double plotas = Math.pow(krastine,2);
        System.out.println("Kvadrato plotas: "+plotas);
    }
}

