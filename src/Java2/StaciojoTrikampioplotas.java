import java.util.Scanner;

public class StaciojoTrikampioplotas {
    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);

        System.out.println("iveskite trikampio plota:");
        double plotas = skaitytuvas.nextDouble();

        System.out.println("Iveskite trikampio ilgi:");
        double ilgis = skaitytuvas.nextDouble();

        //Area = (width*height)/2
        double area = (plotas* ilgis)/2;
        System.out.println("Trikampio plotas: " + area);
    }
}
