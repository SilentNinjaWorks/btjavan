package galutineParduotuve;

public class Preke {
    private Integer prekesId;
    private String tipas;
    private String pavadinimas;
    private Integer kiekis;
    private Double vntkaina;

    public Preke(Integer prekesId, String tipas, String pavadinimas, Integer kiekis, Double vntkaina) {
        this.prekesId = prekesId;
        this.tipas = tipas;
        this.pavadinimas = pavadinimas;
        this.kiekis = kiekis;
        this.vntkaina = vntkaina;
    }

    public Preke(Integer prekesId, String tipas, String pavadinimas, Integer kiekis, Double vntkaina, String tehnologija, String raiska) {
    }

    public Preke(Integer prekesId, String tipas, String pavadinimas, Integer kiekis, Double vntkaina, String procesorius, String ramai, String diskoTalpa) {
    }

    public Preke(Integer prekesId, String tipas, String pavadinimas, Integer kiekis, Double vntkaina, String talpa) {
    }



    @Override
    public String toString() {
        return "Preke{" +
                "prekesId =" + prekesId +
                ", tipas ='" + tipas + '\'' +
                ", pavadinimas ='" + pavadinimas + '\'' +
                ", kiekis =" + kiekis +
                ", vntkaina =" + vntkaina +
                '}';
    }

    public Integer getPrekesId() {
        return prekesId;
    }

    public void setPrekesId(Integer prekesId) {
        this.prekesId = prekesId;
    }

    public String getTipas() {
        return tipas;
    }

    public void setTipas(String tipas) {
        this.tipas = tipas;
    }

    public String getPavadinimas() {
        return pavadinimas;
    }

    public void setPavadinimas(String pavadinimas) {
        this.pavadinimas = pavadinimas;
    }

    public Integer getKiekis() {
        return kiekis;
    }

    public void setKiekis(Integer kiekis) {
        this.kiekis = kiekis;
    }

    public Double getVntkaina() {
        return vntkaina;
    }

    public void setVntkaina(Double vntkaina) {
        this.vntkaina = vntkaina;
    }

}
