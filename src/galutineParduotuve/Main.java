package galutineParduotuve;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Main {

    boolean exit;

    public static void main(String[] args) {

        String failoKelias = new File("").getAbsolutePath()
                +"/src/galutineParduotuve/prekes.txt";
        List<Preke> prekes = new ArrayList<>();
        skaitymas(failoKelias, prekes);
        System.out.println(prekes);

        Main menu = new Main();
        menu.runMain(prekes);

//        PASTABA:
//        MENU PANAUDOJAS VEIKIA TIK RASTI DIDZIAUSIO IR MAZIAUSIO KIEKIO  KOMANDOS ty 1 ir 2 numeriai:
//        KITOS KETURIOS KOMANDOS VEIKIA UZKOMENTAVUS MENIU IR ISNAUDOJA PASPAUDUS RUN
//        PO PIRMO RUNO UZKOMENTAVUS MENIU IRASO I prekes.txt gerai, taiciau antro paleidimo metu  nuskaito null reiksmes
//        IS PRINCIPO VEIKIA VISOS 6 KOMANDOS, TACIAU TIK DVI KOMANDOS SUKELTOS I MENU. KITU KAIP SUKELTI NEZINOJAU.
//


        List<Preke> pirmasKetvirtis = gautiPrekespagaltipa(prekes, 1);
        System.out.println(pirmasKetvirtis);
        List<Preke> antrasKetvirtis = gautiPrekespagaltipa(prekes, 2);
        System.out.println(antrasKetvirtis);
        List<Preke> treciasKetvirtis = gautiPrekespagaltipa(prekes, 3);
        System.out.println(treciasKetvirtis);
        List<Preke> ketvirtasKetvirtis = gautiPrekespagaltipa(prekes, 4);
        System.out.println(ketvirtasKetvirtis);
        pridetiPreke(failoKelias, prekes);
        rasymas(failoKelias, prekes);
        pasalintiPreke(failoKelias, prekes);
        rasymas(failoKelias, prekes);
        redaguotiPreke(failoKelias, prekes);
        rasymas(failoKelias, prekes);

    }

    public static void skaitymas(String failas,List<Preke> prekes) {
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failas))) {
            String eilute = skaitytuvas.readLine();
            while (eilute != null) {
                String[] eilDuomenys = eilute.split(" ");
                if (eilDuomenys[1].equals("televizoriai")) {
                    Integer prekesId = Integer.parseInt(eilDuomenys[0]);
                    String tipas = eilDuomenys[1];
                    String pavadinimas = eilDuomenys[2];
                    Integer kiekis = Integer.parseInt(eilDuomenys[3]);
                    Double vntkaina = Double.parseDouble(eilDuomenys[4]);
                    String technologija = eilDuomenys[5];
                    String raiska = eilDuomenys[6];

                    Preke preke = new Televizorius(prekesId, tipas, pavadinimas, kiekis, vntkaina, technologija, raiska);
                    prekes.add(preke);
                    eilute = skaitytuvas.readLine();
                } else if (eilDuomenys[1].equals("nesiojamas-kompiuteris")) {
                    Integer prekesId = Integer.parseInt(eilDuomenys[0]);
                    String tipas = eilDuomenys[1];
                    String pavadinimas = eilDuomenys[2];
                    Integer kiekis = Integer.parseInt(eilDuomenys[3]);
                    Double vntkaina = Double.parseDouble(eilDuomenys[4]);
                    String procesorius = eilDuomenys[5];
                    String ramai = eilDuomenys[6];
                    String diskoTalpa = eilDuomenys[7];

                    Preke preke = new NesiojamasPC(prekesId, tipas, pavadinimas, kiekis, vntkaina, procesorius, ramai, diskoTalpa);
                    prekes.add(preke);
                    eilute = skaitytuvas.readLine();
                } else if (eilDuomenys[1].equals("skalbykle")) {
                    Integer prekesId = Integer.parseInt(eilDuomenys[0]);
                    String tipas = eilDuomenys[1];
                    String pavadinimas = eilDuomenys[2];
                    Integer kiekis = Integer.parseInt(eilDuomenys[3]);
                    Double vntkaina = Double.parseDouble(eilDuomenys[4]);
                    String talpa = eilDuomenys[5];
                    Preke preke = new Skalbykle(prekesId, tipas, pavadinimas, kiekis, vntkaina, talpa);
                    prekes.add(preke);
                    eilute = skaitytuvas.readLine();
                } else {
                    Integer prekesId = Integer.parseInt(eilDuomenys[0]);
                    String tipas = eilDuomenys[1];
                    String pavadinimas = eilDuomenys[2];
                    Integer kiekis = Integer.parseInt(eilDuomenys[3]);
                    Double vntkaina = Double.parseDouble(eilDuomenys[4]);
                    String talpa = eilDuomenys[5];
                    Preke preke = new Dziovykle(prekesId, tipas, pavadinimas, kiekis, vntkaina, talpa);
                    prekes.add(preke);
                    eilute = skaitytuvas.readLine();
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }
    }

    public static void rasymas(String failoKelias, List<Preke> prekes) {
        try (BufferedWriter rasymas = new BufferedWriter(new FileWriter(failoKelias))) {
            for (int i = 0; i < prekes.size(); i++) {
                rasymas.write(prekes.get(i).toString() + "\n");
                System.out.println(prekes.get(i));
            }
        } catch (FileNotFoundException e) {
            System.out.println("Įrašymo vieta nebuvp rasta");
        } catch (Exception e) {
            System.out.println("Ivyko klaida irasant" + e);
        }
    }



    public static Preke rastiDidzKiekioPreke(List<Preke> prekes) {
        Preke atrinktos = null;
        Integer max = 0;
        for (int i = 0; i < prekes.size(); i++) {
            if (max < prekes.get(i).getKiekis()) {
                max = prekes.get(i).getKiekis();
                atrinktos = prekes.get(i);
            }
        }
        return atrinktos;
    }

    public static Boolean ArYraPrekiu(List<Preke> prekes) {
        if (prekes.size() == 0) {
            System.out.println("Siuo metu prekių nera");
            return false;
        } else {
            return true;
        }
    }



    public static Preke rastiMazKiekioPreke(List<Preke> prekes) {
        Preke atrinktosM = null;
        Integer max = 20;
        for (int i = 0; i < prekes.size(); i++) {
            if (max > prekes.get(i).getKiekis()) {
                max = prekes.get(i).getKiekis();
                atrinktosM = prekes.get(i);
            }
        }
        return atrinktosM;
    }

    public static List<Preke> gautiPrekespagaltipa(List<Preke> prekes, Integer prekesTipas) {
        List<Preke> atrinkti = new ArrayList<>();

        for (Preke preke : prekes) {
            String prekesT = preke.getTipas();
            if (prekesTipas == 1 && (prekesT.equals("televizoriai"))) {
                atrinkti.add(preke);
            } else if (prekesTipas == 2 && (prekesT.equals("nesiojamas-kompiuteris"))) {
                atrinkti.add(preke);
            } else if (prekesTipas == 3 && (prekesT.equals("skalbykle"))) {
                atrinkti.add(preke);
            } else if (prekesTipas == 4 && (prekesT.equals("dziovykle"))) {
                atrinkti.add(preke);
            }
        }
        return atrinkti;
    }






    private static void pasalintiPreke(String failas, List<Preke> prekes) {

        Scanner keyboard = new Scanner(System.in);
        System.out.print("Suveskite prekes ID kuria norite pasalinti ");
        Integer trinamosPrekesId = keyboard.nextInt();

        for (Iterator<Preke> prekesIterator = prekes.iterator(); prekesIterator.hasNext(); ) {
            Preke preke = prekesIterator.next();
            if (preke.getPrekesId().equals(trinamosPrekesId)) {
                prekesIterator.remove();
                System.out.println("Preke buvo pasalinta");
            } else {
                System.out.println("Niekas nebuvo pasalinta");
            }
        }
    }


    private static void redaguotiPreke(String failas, List<Preke> prekes) {
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println(prekes);
        System.out.println("Pasirinkite preke kuria norite redaguoti");
        Integer pasirinkimas = skaitytuvas.nextInt();
        Integer prekePagalIDIndeksas = gautiPrekePagalID(prekes, pasirinkimas);
        if (prekePagalIDIndeksas != null) {
            System.out.println("Iveskite nauja tipa");
            String tipas = skaitytuvas.next();
            prekes.get(prekePagalIDIndeksas).setTipas(tipas);
            System.out.println("Iveskite nauja pavadinima");
            String pavadinimas = skaitytuvas.next();
            prekes.get(prekePagalIDIndeksas).setPavadinimas(pavadinimas);
            System.out.println("Iveskite nauja prekes kieki");
            Integer kiekis = skaitytuvas.nextInt();
            prekes.get(prekePagalIDIndeksas).setKiekis(kiekis);
            System.out.println("Iveskite nauja prekes kaina");
            Double kaina = skaitytuvas.nextDouble();
            prekes.get(prekePagalIDIndeksas).setVntkaina(kaina);
            System.out.println(prekes);
        } else {
            System.out.println("Tokios prekes nera");
        }
    }

    private static void pridetiPreke (String failas, List<Preke> prekes) {
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println("Pasirinkite koki tipa norite prideti");
        System.out.println("1 - televizorius, 2 - nesiojamas kompiuteris, 3 - Skalbykle, 4 - Dziovykle");
        Integer pasirinkimas = skaitytuvas.nextInt();
        String tipas;
        System.out.println("Iveskite prekes id");
        Integer prekeId = skaitytuvas.nextInt();
        System.out.println("Iveskite pavadinima");
        String pavadinimas = skaitytuvas.next();
        System.out.println("Iveskite kieki");
        Integer kiekis = skaitytuvas.nextInt();
        System.out.println("Iveskite vnt. kaina");
        Double vntKaina = skaitytuvas.nextDouble();
        if (pasirinkimas == 1) {
            tipas = "televizoriai";
            System.out.println("Iveskite televizoriaus technologija");
            String technologija = skaitytuvas.next();
            System.out.println("Iveskite televizoriaus raiska");
            String raiska = skaitytuvas.next();
            Televizorius naujasTelevizorius = new Televizorius(1, tipas, pavadinimas, kiekis, vntKaina, technologija, raiska);
            prekes.add(naujasTelevizorius);
            System.out.println(naujasTelevizorius.toString());
            // panaudoti ta pati rasymo metoda
        } else if (pasirinkimas == 2) {
            tipas = "nesiojamas-kompiuteris";
            System.out.println("Iveskite Nesiojamo kompiuterio procesoriu");
            String procesorius = skaitytuvas.next();
            System.out.println("Iveskite nesiojamo kompiuterio ramai");
            String ramai = skaitytuvas.next();
            System.out.println("Iveskite nesiojamo kompiuterio ramai");
            String diskoTalpa = skaitytuvas.next();
            NesiojamasPC naujasNesiojamasPC = new NesiojamasPC(2, tipas, pavadinimas, kiekis, vntKaina, procesorius, ramai, diskoTalpa);
            prekes.add(naujasNesiojamasPC);
            System.out.println(naujasNesiojamasPC.toString());
            // panaudoti ta pati rasymo metoda
        } else if (pasirinkimas == 3) {
            tipas = "skalbykle";
            System.out.println("Iveskite skalbykles talpa");
            String talpa = skaitytuvas.next();
            Skalbykle naujaSkalbykle = new Skalbykle(3, tipas, pavadinimas, kiekis, vntKaina, talpa);
            prekes.add(naujaSkalbykle);
            System.out.println(naujaSkalbykle.toString());
            // panaudoti ta pati rasymo metoda
        } else if (pasirinkimas == 4) {
            tipas = "dziovykle";
            System.out.println("Iveskite dziovykles talpa");
            String talpa = skaitytuvas.next();
            Dziovykle naujaDziovykle = new Dziovykle(4, tipas, pavadinimas, kiekis, vntKaina, talpa);
            prekes.add(naujaDziovykle);
            System.out.println(naujaDziovykle.toString());
            // panaudoti ta pati rasymo metoda
        }
    }







    public static Integer gautiPrekePagalID(List<Preke> prekes,
                                            Integer id) {
        for (int i = 0; i < prekes.size(); i++) {
            if (prekes.get(i).getPrekesId().equals(id)) {
                return i;
            }
        }
        return null;
    }


    private void spaudintiHeader() {
        System.out.println("-----------------------------");
        System.out.println("       Sveiki Atvyke i Algirdo     ");
        System.out.println("          Parduotuve               ");
        System.out.println("-----------------------------");
    }

    private void atspausdintiMenu() {
        System.out.println("Prasome suvesti jusu pasirinkima: ");
        System.out.println("1. Rasti prekę, kurios vienetų turi mažiausiai ir ją atspausdinti konsolėje: ");
        System.out.println("2. Rasti prekę, kurios vienetų turi daugiausiai ir ją atspausdinti konsolėje: ");
        System.out.println("3. Išfiltruoti prekes pagal tipą ir atspausdinti į failą atfiltruotosPrekes.txt: ");
        System.out.println("4. Pridėti prekę prie esančio sąrašo ir tą sąrašą išsaugoti prekes.txt faile: ");
        System.out.println("5. Ištrinti prekę pagal prekės id ir tą sąrašą išsaugoti prekes.txt faile: ");
        System.out.println("6. Redaguoti prekę pagal prekės id ir tą sąrašą išsaugoti prekes.txt faile: ");
        System.out.println("7. Iseiti is menu");
    }

    public void runMain(List<Preke> prekes) {
        spaudintiHeader();
        while(!exit) {
            atspausdintiMenu();
            int pasirinkimas = getInput();
            performAction(pasirinkimas, prekes);
        }
    }

    private int getInput() {
        Scanner skaitytuvas = new Scanner(System.in);
        int pasirinkimas = -1;
        while (pasirinkimas < 0 || pasirinkimas > 7) {
            try {
                System.out.println("\n Suveskite savo pasirinkima: ");
                pasirinkimas = Integer.parseInt(skaitytuvas.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Netinkamas pasirinkimas");
            }
        }
        return pasirinkimas;
    }

    private void performAction(int pasirinkimas, List<Preke> prekes) {
        switch(pasirinkimas) {
            case 0:
                exit = true;
                System.out.println("Aciu, kad pirkote");
                break;
            case 1 : {
                if (ArYraPrekiu(prekes))
                System.out.println("Mažiausiai vnt turi si preke: " + rastiMazKiekioPreke(prekes));
            }
                break;
            case 2: {
                if (ArYraPrekiu(prekes))
                    System.out.println("Didziausiai vnt turi si preke: " + rastiDidzKiekioPreke(prekes));
            }
                break;
            case 3:
            {

            }
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            case 7:
                break;
            default:
                System.out.println("Jusu medinis kompiuteris");
        }
    }



}
