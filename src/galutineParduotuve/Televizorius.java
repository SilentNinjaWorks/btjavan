package galutineParduotuve;

public class Televizorius extends Preke {
    private String technologija;
    private String raiska;

    public Televizorius(Integer prekesId, String tipas, String pavadinimas, Integer kiekis, Double vntkaina, String technologija, String raiska) {
        super(prekesId, tipas, pavadinimas, kiekis, vntkaina);
        this.technologija = technologija;
        this.raiska = raiska;
    }

    @Override
    public String toString() {
        return "Televizorius{" + "prekesId=' " + getPrekesId() + " pavadinimas=' " + getPavadinimas() + " kiekis=' " + getKiekis() + " vntkaina=' " + getVntkaina() +
                " technologija='" + technologija + '\'' +
                ", raiska='" + raiska + '\'' +
                '}';
    }

    public String getTechnologija() {
        return technologija;
    }

    public void setTechnologija(String technologija) {
        this.technologija = technologija;
    }

    public String getRaiska() {
        return raiska;
    }

    public void setRaiska(String raiska) {
        this.raiska = raiska;
    }
}


