package galutineParduotuve;

public class NesiojamasPC extends Preke {
    private String procesorius;
    private String ramai;
    private String diskoTalpa;

    public NesiojamasPC(Integer prekesId, String tipas, String pavadinimas, Integer kiekis, Double vntkaina, String procesorius, String ramai, String diskoTalpa) {
        super(prekesId, tipas, pavadinimas, kiekis, vntkaina);
        this.procesorius = procesorius;
        this.ramai = ramai;
        this.diskoTalpa = diskoTalpa;
    }

    @Override
    public String toString() {
        return "NesiojamasPC{" + "prekesId=' " + getPrekesId() + " pavadinimas=' " + getPavadinimas() + " kiekis=' " + getKiekis() + " vntkaina=' " + getVntkaina() +
                "procesorius='" + procesorius + '\'' +
                ", ramai='" + ramai + '\'' +
                ", diskoTalpa='" + diskoTalpa + '\'' +
                '}';
    }

    public String getProcesorius() {
        return procesorius;
    }

    public void setProcesorius(String procesorius) {
        this.procesorius = procesorius;
    }

    public String getRamai() {
        return ramai;
    }

    public void setRamai(String ramai) {
        this.ramai = ramai;
    }

    public String getDiskoTalpa() {
        return diskoTalpa;
    }

    public void setDiskoTalpa(String diskoTalpa) {
        this.diskoTalpa = diskoTalpa;
    }
}
