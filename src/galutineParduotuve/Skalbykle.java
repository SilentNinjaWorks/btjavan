package galutineParduotuve;

public class Skalbykle extends Preke {
    private String talpa;

    public Skalbykle(Integer prekesId, String tipas, String pavadinimas, Integer kiekis, Double vntkaina, String talpa) {
        super(prekesId, tipas, pavadinimas, kiekis, vntkaina);
        this.talpa = talpa;
    }

    @Override
    public String toString() {
        return "Skalbykle{" + "prekesId=' " + getPrekesId() + " pavadinimas=' " + getPavadinimas() + " kiekis=' " + getKiekis() + " vntkaina=' " + getVntkaina() +
                "talpa='" + talpa + '\'' +
                '}';
    }

    public String getTalpa() {
        return talpa;
    }

    public void setTalpa(String talpa) {
        this.talpa = talpa;
    }


}
